import java.util.Scanner;

public class TowerOfHanoi {
	
	public static void main(String[] args){
		
		Scanner input = new Scanner(System.in);
		System.out.print("Oppgi antall skiver: ");
		int n = input.nextInt();
		
		System.out.println("Trekkene er: ");
		moveDisks(n, 'A','B', 'C');
	}
	static int teller = 0;
	public static void moveDisks(int n, char fromTower, char toTower, char auxTower) {
		boolean firstAndLast = false;
		if (teller == 0)
			firstAndLast = true;
		teller += 1;
			
		if (n ==1) {
			System.out.println("Flytt skive " +n+ " fra " + fromTower +" til " + toTower);
		}
		else {
			moveDisks(n-1,fromTower,auxTower,toTower);
			System.out.println("Flytt skive " +n+ " fra " + fromTower +" til " + toTower);
			moveDisks(n-1,auxTower,toTower,fromTower);
		}
		if(firstAndLast) {
			System.out.println("Antall trekk: " + teller);
			System.out.println("Antall kall til den rekursive metoden: " + teller);
		}
	}
	
	
}
